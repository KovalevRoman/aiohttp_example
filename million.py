'''
Скрипт обрабатывает файл файл с именами доменов для проверки
Для каждого имени домена из файла он асинхронно (с помощью aiohttp)
делает попытку скачать главную страницу сайта по адресу имени домена.
Выдыделяет из HTML только текст, удаляет все HTML-теги,
а в начало текста (в первые несколько строк)
добавляет (выделеные из HTML) TITLE страницы и meta:keywords и meta:description.
Сохраняет получившийся файл в каталог downloads/ с именем {domainname}.txt
В конце скрипта пишется CSV-файл с тремя колонками:
1. Имя домена.
2. Код статуса, выданного web-сервером при попытке скачать страницу
    или NULL, если не удалось отрезолвить имя домена или сервер не отвечает.
3. Флаг 0 или 1: удалось ли скачать главную страницу сайта.

Для работы с csv файлом используется библиотека Pandas.
Для асинхронных операций используются библиотеки asyncio и aoihttp.
Для работы со структурой html используется библиотека Beautiful Soup.

reqirements.txt - файл с зависимостями (например, для pip).
Перед запуском скрипта требуется увеличить разрешенное число одновременно открытых файлов ОС.
Для *nix: ulimit -n Х , где Х = QUANTITY+10%

Автор: Роман Ковалев (rvk.sft@gmail.com)
'''

import pandas as pd
import time
import asyncio
from aiohttp import TCPConnector
from aiohttp import ClientSession
from bs4 import BeautifulSoup

# Количество одновременно выполняющихся задач
QUANTITY = 2000

# Сайты могут отдавать разные страницы для разных user-agent (живой пользователь или бот, например).
# Выбран как у обычного поесетителя сайта.
useragent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36'

filename = 'domains-list.csv'

# Считываю csv в Pandas.DataFrame и создаю итератор по строкам
df = pd.read_csv(filename)
urls = df.iterrows()

start = time.time()

async def fetch(url, session):
    try:
        async with session.get(url) as response:
            print(("Fetching url: {}").format(url))
            return await response.read(), response.url, response.status
    except Exception as e:
        return url.split('/')[2], 'NULL', 0


async def bound_fetch(sem, session):
    async with sem:
        url = 'http://' + next(urls)[1].values.tolist()[1]
        try:
            # Выполняю fetch(), получаю и обрабатываю данные из ответа
            text, domainname, status = await fetch(url, session)
            domainname = str(domainname).split('/')[2].split(':')[0]
            soup = BeautifulSoup(text, 'lxml')
            try:
                title = soup.find('title').get_text()
            except Exception as e:
                title=''
            try:
                keywords = soup.find('meta', {'name':'keywords'})['content']
            except Exception as e:
                keywords = ''
            try:
                description = soup.find('meta', {'name':'description'})['content']
            except Exception as e:
                description = ''
            try:
                raw_text = soup.find('body').get_text()
                success = 1
            except Exception as e:
                raw_text = ''
                success = 0
            header = '\n'.join((title, description, keywords))
            with open('downloads/' + domainname + '.txt', 'w') as file:
                file.write(header + raw_text)
        except Exception as e:
            domainname = url.split('/')[2]
            status = 'NULL'
            success = 0

        return domainname, status, success


async def run(size):
    tasks = []
    # Создаю семафор для ограничения количества одновременно выполняющихся задач
    sem = asyncio.Semaphore(QUANTITY)

    # Один ClientSession для всех запросов, TCPConnector(verify_ssl=False) отключает проверку sll сертификата
    async with ClientSession(
            headers={'User-Agent': useragent},
            connector=TCPConnector(
                verify_ssl=False,
                limit=1000
            )
    ) as session:
        for i in range(size):
            task = asyncio.ensure_future(bound_fetch(sem, session))
            tasks.append(task)
        responses = await asyncio.gather(*tasks)

    # Сохраняю косолидированную информацию из сопрограмм в файл
    df = pd.DataFrame.from_records(responses)
    df.to_csv('file.csv')

# Подсчитываю количество доменов в файле
size = df.count()[1]

# Создаю объект цикла событий
loop = asyncio.get_event_loop()

# Создаю task из со-программы и отправил на исполнение в цикл событий
future = asyncio.ensure_future(run(size))
loop.run_until_complete(future)

print("Script spent: {:.2f} seconds".format(time.time() - start))
